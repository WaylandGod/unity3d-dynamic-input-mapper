using UnityEngine;
using System.Collections;

public class ChestInteract : MonoBehaviour, IInteract
{
    bool interact = false;
	// Update is called once per frame
	void Update () {
        if (interact && ((CharacterMotor)GameObject.Find("Player").GetComponent(typeof(CharacterMotor))).movement.velocity != Vector3.zero)
        {
            interact = false;
            animation.CrossFade("Close");
            //((Animator)this.GetComponent(typeof(Animator))).SetBool("Interact", false);
        }
	}
    
    public void Interact(GameObject go)
    {
        Debug.Log(go.name);
        
        if (!interact && ((CharacterMotor)GameObject.Find("Player").GetComponent(typeof(CharacterMotor))).movement.velocity == Vector3.zero)
        {
            //((Animator)this.GetComponent(typeof(Animator))).SetBool("Interact", true);
            animation.Play("Open");
            interact = true;
        }
    }
}
